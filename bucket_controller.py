from minio import Minio
from minio.error import S3Error

class Bucket():
    try:
        client = Minio("play.min.io",
            access_key="minioadmin",
            secret_key="minioadmin")
    except S3Error as exc:
        print("error occurred.", exc)

    def check_bucket(self):
        client = self.client
        found = client.bucket_exists("atom")
        if not found:
            client.make_bucket("atom")
            
    def upload(self, file, objectName):
        client = self.client
        client.put_object(
            "atom", objectName, file, 5)

    def get_odjects_list(self):
        client = self.client
        objects = client.list_objects('atom', recursive=True)
        return objects

    def get_objects_in_backet(self):
        client = self.client
        objects = client.list_objects('atom', recursive=True)
        response = {}
        count = 1
        for obj in objects:
            file_name = str(obj.object_name.encode('utf-8'))[2:-1]
            response["object_" + str(count)] = {"file_name": file_name, "time_created": str(obj.last_modified)}
            count +=1
        return response

    def remove(self, obj):
        client = self.client
        client.remove_object("atom", obj, version_id=None)
        return {"status": 200}

