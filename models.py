from sqlalchemy import Column, DateTime, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import func

Base  = declarative_base()

class Picture(Base):
    __tablename__ = 'inbox'
    code = Column(String)
    name = Column(String, primary_key=True, index=True)
    time_created = Column(DateTime(timezone=True), server_default=func.now())





