import uuid
from xml.etree.ElementTree import tostring
from typing import List
from fastapi import FastAPI, HTTPException, UploadFile, File, Depends
from bucket_controller import Bucket
from session import SessionLocal, engine 
from models import Base
import crud

Base.metadata.create_all(bind=engine)

app = FastAPI()

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
	
async def to_bytecode(name):
	with open(name.file, "rb") as img:
		f = img.read()
		uploading_file = bytearray(f)
	return uploading_file

@app.get('/')
def hi():
	return {"greeting": "HI Greenatom!!!"}

@app.get('/all_pictures')
def get_all_pictures():
	bucket = Bucket()
	response = bucket.get_objects_in_backet()
	return response


@app.post("/create_picture")
async def create_picture(files: List[UploadFile] = File(...)):
	bucket = Bucket()
	response = {}
	count = 1
	if len(list(files)) > 15:
		raise HTTPException(status_code=400, detail="There are more than 15 objects")
	for file in files:
		newName= f"{uuid.uuid4()}.jpg"
		db_picture = crud.create_picture(name_new=newName)
		obj = {"code": db_picture.code, "name": db_picture.name, "time_created": db_picture.time_created}
		key = "picture_" + str(count)
		response[key] = obj
		count += 1
		uploading_file = file.file
		bucket.upload(objectName=db_picture.name, file=uploading_file )
	return {"object": response}

@app.delete('/{picturename}')
async def delete_picture(picturename):
	crud.delete_picture(name=picturename)
	bucket = Bucket()
	objects = bucket.get_odjects_list()
	for obje in objects:
		print(str(obje.object_name.encode('utf-8'))[2:])
		if str(obje.object_name.encode('utf-8'))[2:-1] == picturename:
			bucket.remove(obje.object_name.encode('utf-8'))

