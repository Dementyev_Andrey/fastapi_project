FROM python:3.10-slim

RUN apt-get update && apt-get install -y \
    curl \
&& rm -rf /var/lib/apt/lists/*

COPY requirements.txt /tmp/
RUN pip install --no-cache-dir -r /tmp/requirements.txt

RUN useradd --user-group -ms /bin/bash app

ENV PYTHONUNBUFFERED=1
ENV HOME=/home/app
ENV LOGS_DIR=$HOME/logs
ENV PATH=$PATH:/home/app/.local/bin

WORKDIR $HOME
COPY --chown=app:app . $HOME