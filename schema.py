from datetime import datetime
from pydantic import BaseModel

class Picture_s(BaseModel):
    code: str
    name: str
    date: datetime

    class Config:
        orm_mode = True


