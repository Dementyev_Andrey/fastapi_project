from fastapi import HTTPException
from sqlalchemy.orm import Session
from models import Picture
from datetime import datetime
from session import SessionLocal, engine 

def create_picture(name_new) :
    db = SessionLocal()
    db_picture = Picture(code="POST", name=name_new, time_created=datetime.now())
    db.add(db_picture)
    db.commit()
    db.refresh(db_picture)
    return db_picture

def delete_picture(name):
    db = SessionLocal()
    picture = db.get(Picture, name)
    if not picture:
        raise HTTPException(status_code=404, detail="picture not found")
    db.delete(picture)
    db.commit()
    return {"ok": True}